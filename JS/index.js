"use strict"
function filterBy(a, b) {
    let arrFiltered;
    if (a === null) {
        arrFiltered = a.filter(number => typeof number !== 'object');
    } else {
        arrFiltered = a.filter(number => typeof number !== b);
    }
    return arrFiltered;
}

console.log(filterBy(['hello', 'world', 23, '23', null], 'string'));

